package com.cleevio.foldermanager.Model.CustomViews;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TimeFormatException;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by filas on 22.10.2016.
 */

public class CustomEditTextPreference extends EditTextPreference {

    public CustomEditTextPreference(Context context) {
        super(context);
    }

    public CustomEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditTextPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            findPreferenceInHierarchy("edittext_preference").getEditor();
        }
        super.onClick(dialog, which);
    }



}
