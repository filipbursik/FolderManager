package com.cleevio.foldermanager.Model.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cleevio.foldermanager.Model.Entities.Folder;
import com.cleevio.foldermanager.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by filas on 21.10.2016.
 */

public class GridAdapter extends BaseAdapter {

    public interface OnItemClickListener {
        void onItemClick(Folder item, View view, int position);
    }

    public interface OnLongItemClickListener {
        void onItemLongClick(Folder item);
    }

    private List<Folder> folder;
    private List<Integer> deleleFolder;

    private Context context;

    private final OnItemClickListener listener;
    private final OnLongItemClickListener listenerLong;

    private static LayoutInflater inflater = null;

    public GridAdapter(List<Folder> folderList, OnItemClickListener listener, OnLongItemClickListener listenerLong,Context context, List<Integer> deleleFolder) {
        this.listener = listener;
        this.folder = folderList;
        this.context = context;
        this.deleleFolder = deleleFolder;
        this.listenerLong = listenerLong;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return folder.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView tv;
        ImageView img;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.item_grid, null);
        holder.tv = (TextView) rowView.findViewById(R.id.textView1);
        holder.img = (ImageView) rowView.findViewById(R.id.imageView1);

        holder.tv.setText(folder.get(position).getName());
        holder.img.setImageDrawable(folder.get(position).getImage());

        if (deleleFolder.contains(position)) {
           rowView.setBackgroundColor(Color.GRAY);
        }

        rowView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listenerLong.onItemLongClick(folder.get(position));
                return false;
            }
        });

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(folder.get(position), view, folder.indexOf(folder));
            }
        });

        return rowView;
    }

}
